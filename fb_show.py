#!/usr/bin/python3

from tkinter import *
from buffer import bf

root=Tk()
root.title("ST7525 framebuffer emulator")
root.resizable(False, False)

c = Canvas(width=650, height=150, bg='white')
c.create_rectangle(10,10,160*4+4,10+32*4, fill='grey', outline='grey')


def btn_left_cb():
    print("Left")

def btn_right_cb():
    print("Right")

def draw_framebuffer(fb):

    x1 = 10
    x2 = 14
    y_offset = 0

    for v_line in fb:
        for i in range(0,7):

            y1 = i * 4 + 10 + y_offset
            y2 = y1 + 4

        # reset counter on new line
            if(x1 >= (160 * 4 + 10)):
                y_offset = y_offset + 29;
                x1 = 10
                x2 = 14

            if(v_line & (1<<i)):
                # c.create_rectangle(x1,y1,x2,y2,fill='black',outline='grey')
                c.create_rectangle(x1,y1,x2,y2,fill='black')
        x1=x1+4
        x2=x2+4

draw_framebuffer(bf)

c.pack()
root.mainloop()

