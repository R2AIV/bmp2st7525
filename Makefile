all: 
	gcc -o bmp2pwrm bmp2pwrm.c

run: all conv copy visualize

clean:
	rm ./bmp2pwrm

conv: 
	./bmp2pwrm analyse.bmp output.dat

copy: 
	cp output.dat /mnt/c/users/sovkods

edit:
	vim bmp2pwrm.c

visualize:
	python3 ./fb_show.py

