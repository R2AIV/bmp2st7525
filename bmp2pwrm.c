#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>

/*
 *
 * BMP2PWRM - Monochrome BMP 160x32 (only!) image converter.
 * Converts BMP image to ST7525 display framebuffer.
 * Written by R2AIV 100222
 * 
 */

FILE *BMP_InputData;
FILE *PWR_OutputData;
FILE *BMP_OutputData;
FILE *PythonFile;

uint8_t InputBuf[640];
uint8_t OutputBuf[640];
uint8_t BmpFileOutputBuf[640];

const uint8_t BMP_Header[] = {0x42, 0x4D, 0xBE, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x28, 0x00,
                              0x00, 0x00, 0xA0, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00,
                              0x00, 0x00, 0x80, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00 ,0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                              0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00};

#define PIXELS_IN_ROW 160
#define PIXELS_IN_COL 32

// Get's one pixel state from BMP 160x32 data buffer
uint8_t GetPixelFromBMP(uint8_t *ptr, int x, int y)
{
  uint8_t PixelState = 0;
  PixelState = ptr[x / 8 + y * PIXELS_IN_ROW / 8] & (1 << (7 - (x % 8))) ? 0 : 1;
	return PixelState;
};

uint8_t GetPixelFromBuf(uint8_t *ptr, int x, int y)
{
  uint8_t PixelState = 0;
  PixelState = (ptr[x + y / 8 * PIXELS_IN_ROW] & (1 << (y % 8))) ? 0 : 1;
  return PixelState;
}

// Puts one pixel to ST7525 160x32 data buffer
void PutPixelToBuf(uint8_t  *ptr, int x, int y, uint8_t state)
{
  if(state) ptr[x + y / 8 * PIXELS_IN_ROW] |= (1 << ((y % 8)));
};

void PutPixelToBMP(uint8_t *ptr, int x, int y, uint8_t state)
{
  if(state) (ptr[x / 8 + y * PIXELS_IN_ROW / 8] |= (1 << (7 - (x % 8))));
}

void ConvertBMP_To_ST7525(uint8_t *Src, uint8_t *Dst)
{
  int x,y;
  for(x=0;x<160;x++)
    for(y=0;y<32;y++)
      PutPixelToBuf(Dst, x, 32-y, GetPixelFromBMP(Src,x,y));
}


void ConvertST7525_To_BMP(uint8_t *Src, uint8_t *Dst)
{
  int x,y;
  for(x=0;x<160;x++)
    for(y=0;y<32;y++)
    {
      PutPixelToBMP(BmpFileOutputBuf, x, 31-y, GetPixelFromBuf(OutputBuf,x,y));
    }
}

int main(int argc, char **argv)
{
	uint8_t TmpSymbIn[8] = {0};
	uint8_t TmpSymbOut[8] = {0};

	int InputBufPtr = 0;
	int OutputBufPtr = 0;

	if(argc != 3) 
	{
		printf("USAGE: bmp2pwrm [IN_FILE] [OUT_FILE]\r\n");
		return -1;
	};

	memset((void *)OutputBuf,0,sizeof(OutputBuf));

	BMP_InputData = fopen(argv[1],"r");

	// Dummy read header
	for(int i=0;i<=61;i++) fgetc(BMP_InputData);

	// Fill the input buffer by raster data
	for(int i=0;i<=639;i++) 
		InputBuf[i] = fgetc(BMP_InputData);

	fclose(BMP_InputData);

  ConvertBMP_To_ST7525(InputBuf, OutputBuf);

  // Reverse conversion test
  BMP_OutputData = fopen("output.bmp", "w");
  for(int i=0;i<sizeof(BMP_Header);i++) 
    fputc(BMP_Header[i], BMP_OutputData);

  ConvertST7525_To_BMP(OutputBuf, BmpFileOutputBuf);

  for(int i=0;i<sizeof(BmpFileOutputBuf);i++)
    fputc(BmpFileOutputBuf[i], BMP_OutputData);

  fclose(BMP_OutputData);

	PWR_OutputData = fopen(argv[2],"w");
	PythonFile = fopen("buffer.py","w");
	fprintf(PythonFile,"bf = [");

	for(int i=0;i<sizeof(OutputBuf);i++) 
	{
		fputc(OutputBuf[i],PWR_OutputData);
		fprintf(PythonFile, "0x%.2X", OutputBuf[i]);
		if(i!=639) fprintf(PythonFile, ", ");
		if((i % 16 == 0) && (i != 0)) fprintf(PythonFile,"\r\n      ");
	};

	fprintf(PythonFile,"]\r\n\r\n");
	fclose(PythonFile);
	fclose(PWR_OutputData);

	return 0;
}
